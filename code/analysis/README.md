### Files
The cvs files contain all the data necessary to reproduce the figure in the paper. The ipynb files contain the code used to plot every graphics in the figures. There is one code file per figure.
### Instructions
Open the .ipynb files with jupyter notebook (you can download it here: https://jupyter.org/).
If you run every cells, it will automatically call the right csv file and plot the associeted graphics.

### [Main repository](https://gitlab.com/slcu/teamhj/publications/durand_etal_2019)
