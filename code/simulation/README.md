### Overview
The simulations use tubulaton available from https://gitlab.com/slcu/teamHJ/tubulaton. This is a C++ implemented programme for modelling the cytoskeleton. The link contains details about downloading and running tubulaton which will be necessary to run these simulations. The scripts listed here are also available in the folder https://gitlab.com/slcu/teamHJ/tubulaton/script/DurandEtAl which should download with tubulaton and can be used to run these simulations too. 

The simulations are run using bash scripts which call tubulaton, and Python scripts are used for generating the .ini input file for tubulaton, data analysis and plotting. These files are in the subdirectories. 

### [Main repository](https://gitlab.com/slcu/teamhj/publications/durand_etal_2019)

### Main scripts : Bash Files For Running Simulations 
The below bash files can be run on the command line to generate the data for the main figures in the paper. In brief, they create missing directories, set all parameter values for the simulation, generate a .ini input file for tubulaton, update the ini file for repition number, then run this .ini file using tubulaton to generate the necessary data and finally calculate anisotropy. The directories of PROJECTDIR (line 8) will need updating updating for the location of where you installed tubulaton. Furthermore directories of DDIR, HDIR,WORKDIR(0-4)  on line 9-15 may need updating for your file system.

- Fig2\_Rectangle and Fig2_Square generate the simulation data in the square and rectangular domain for Fig 2d-f of Durand et. al. 
- Fig4\_Square generates the Wild Type and Katanin simulation data for Fig 4d-f of Durand et. al. The variable CutList on Line 97 determines the probability of severing.  
- Rectangle_Extension and Square_Extension generate the data in square and rectangular domain for the figures in the supplementary information, Fig S8 and the wild-type simulations of Fig. S11.  Note: as stated in the paper the code takes a long time for larger nucleation rates so the last four cases of parameters set in line 100-104 may take a long time to run.
- Rectangle_Extension_Kat and Square_Extension_Kat generate the data in square and rectangular domain for the katanin simulations of Fig. S11.  Note: as stated in the paper the code takes a long time for larger nucleation rates so the last four cases of parameters set in line 100-104 may take a long time to run.

### Python Files for Running Simulations  : 
There are three python files needed to run the above bash scripts. All these scripts must be in the folder listed as WORKDIR2. These three files

- Generate\_Init\_File.py . _This takes the given input parameters and writes the input file for tubulaton_
- config_generator\_initrepetition.py . _Multiple repetitions are done with the same simulation parameters and the .ini file needs minor changes to keep each repeat distint. This file makes those changes_
- CalculateAnisotropy. _This caluclates the anisotropy and directional statistic and saves that data (and some of the parameters of the simulation) to a single line in a text file. _ 

### Plotting Supplementary Figures
The supplmentary figures are plotted using the bash script PlottingOnly. This contains the location of the anisotropy data which will be plotted then immediately calls the python script PlotCutAgainstAnisotropy_ExtraFigures_1. PlottingOnly generates the plots of Fig S8 and S11. It needs to be run seperately for the square and rectangular domains. 

The simulation graphs in the Fig.2 and 4 were plotted using the same scripts as the experimental data so not included in this folder. 

The images of the cytoskeleton network in Fig 2d and 4d are visualied in paraview. The rectangular and square .vtk strucutres are read into paraview and made partially transparent.  Then, we additionally read in the .vtk output from tubulaton and viewed using a glyph. (See tubulaton gitlab page for further details on visualising output in paraview.)
