#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
from tempfile import mkstemp
from shutil import move
from os import fdopen, remove

def replace(file_path, pattern, subst, rep_num):
    """ In the .init file it replaces the _repeat_n for n the previous reptition number with _repeat_n for n the current repition number in the output file name. If needing to insert this for the first time as it is the first repition it does so immediately before the comment marked by # or before the /n end of line marker. Note is updates the init file by creating a new file and copying all lines across, updating the appropraite line, then replacing the origional file withe the new one. 
    Inputs: file_path init file anem with directory, pattern is the sequence to be replaces, substr is hte sequence to replace pattern with, rep_num is the number of the repition.  
    """
    #Create temp file
    fh, abs_path = mkstemp()
    with fdopen(fh,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                if 'nom_output_vtk' in line:
                    index=line.find('#')
                    if index==-1:
                        index=line.find('/n')
                    if pattern in line:
                        x=line.split('_')
                        pos=x.index(pattern)
                        pattern_new="_"+pattern+"_"+x[pos+1]+"_"
                        new_file.write(line.replace(pattern_new, subst))
                    else :
                        if rep_num!=1:
                            print("Warning in config_generator_initrepition.py: No previous repitions but reptition not equal to 1")
                        new_file.write(line[:index]+subst+line[index:])
                        print(line+subst)
                else :
                    new_file.write(line)
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)


def main():
    """
    Called in /script_multi_runs. Used when doing multiple repitions of the same simulation. This updates the init file so the output directory changes so each result is printed to different file
    Inputs: file name including directory of the init file to update, the number of this reptition to input in output file name.
    """
    init_file=sys.argv[1]
    rep_num=int(sys.argv[2])
    print(rep_num)
    temp=int(rep_num)-1
    pattern='repeat'
    subst='_repeat_'+str(rep_num)+'_'

    replace(init_file,pattern,subst,rep_num)


if __name__ == '__main__':
    main()









